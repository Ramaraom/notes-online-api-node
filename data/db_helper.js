

exports.error = (code,message) => {
    var e = new Error(message);
    e.code = code;
    return e;
}

exports.missing_data = (field) => {
    return exports.error("missing_data","please provide data : "+field);
}
exports.no_user = () => {
    return exports.error("no_user","Provided user does not exist");
}
exports.already_registered = () => {
    return exports.error("already_registered","Provided user is already registered");
}
exports.db_error = () => {
    return exports.error("db_error","Error has occurred while accessing database");
}
exports.valid_filename = (fn) => {
    var re = /[^\.a-zA-Z0-9_-]/;
    return typeof fn == 'string' && fn.length > 0 && !(fn.match(re));
}
exports.file_error = (err) => {
   return exports.error("file_error",JSON.stringify(err.message));
}
exports.verify = (data, field_names) => {
    for(vari=0;i<field_names.length;i++){
        if(!data[field_names[i]]){
            throw exports.error("missing_data",field_names[i]+" not optional");
        }
    }
}
exports.file_copy = function () {

    var src, dst, callback;
    var can_overwrite = false;

    if (arguments.length == 3) {
        src = arguments[0];
        dst = arguments[1];
        callback = arguments[2];
    } else if (arguments.length == 4) {
        src = arguments[0];
        dst = arguments[1];
        callback = arguments[3];
        can_overwrite = arguments[2]
    }

    function copy(err) {
        var is, os;
 
        if (!err && !can_overwrite) {
            return callback(backhelp.error("file_exists",
                                     "File " + dst + " exists."));
        }
 
        fs.stat(src, function (err) {
            if (err) {
                return callback(err);
            }

            is = fs.createReadStream(src);
            os = fs.createWriteStream(dst);
            is.on('end', function () { callback(null); });
            is.on('error', function (e) { callback(e); });
            is.pipe(os);
        });
    }
    
    fs.stat(dst, copy);
};



function now_in_s() {
    return Math.round((new Date()).getTime() / 1000);
}