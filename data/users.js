let async = require('async'),
    db = require('./db'),
    db_helper = require('./db_helper'),
    bcrypt = require('bcrypt'),
    uuid = require('node-uuid');

exports.version = "0.1.0";
function user_by_field(field_name,field_value,callback){
    var obj = {};
    obj[field_name]=field_value;
    db.users.find(obj).toArray((err,results) => {
        if(err){
            callback(err);
            return;
        }
        if(results.length == 0){
            callback(null,null);
        }
        else if(results.length == 1){
            callback(null,results[0])
        }
        else{
            callback(db_helper.db_error());
        }
    });
}
function getUserById(uuid, callback) {
    if (!uuid)
        callback(db_helper.missing_data("user_id"))
    else {
        user_by_field("id", uuid,callback);
    }
}

function getUserByEmail(email,callback) {
    if (!email)
        callback(db_helper.missing_data("email"))
    else {
        user_by_field("email_address", email,callback);
    }
}

function getUserByName(name,callback) {
    if (!name)
        callback(db_helper.missing_data("name"))
    else {
        user_by_field("name", name,callback);
    }
}

function register(data, callback) {

    async.waterfall([
        (cb) => {
            if (!data.email) {
                cb(db_helper.missing_data("email"));
            }
            else if (!data.name) {
                cb(db_helper.missing_data("name"));
            }
            else if (!data.password) {
                cb(db_helper.missing_data("password"));
            }
            else {
                bcrypt.hash(data.password, 10, cb);
            }
        },
        (hash, cb) => {
            let userid = uuid();
            var user = {
                _id: email_address,
                userid: userid,
                name: data.name,
                email_address: data.email,
                password: hash,
                deleted: false,
                created_dt: now_in_s(),
                last_modified_dt: now_in_s()
            }
            db.users.insertOne(user, { w: 1, safe: true }, cb);
        },
        (results, cb) => {
            cb(null, results[0]);
        }
    ], function (err, user_data) {
        if (err) {
            if (err instanceof Error && err.code == 11000)
                callback(db_helper.already_registered());
            else
                callback(err);
        } else {
            callback(null, user_data);
        }
    });

}
exports.getUserByEmail = getUserByEmail;
exports.getUserById = getUserById;
exports.getUserByName = getUserByName;
exports.register = register;

