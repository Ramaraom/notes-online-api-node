let async = require('async'),
    db = require('./db'),
    db_helper = require('./db_helper')

exports.version = "0.1.0";

exports.getAllNotes = (skip, count, callback) =>{
    db.notes.find().
             limit(count).
             skip(skip).
             toArray(callback);
}

exports.getNoteById = (id,callback) => {
    db.notes.findOne({id:id},callback);
}

exports.createNote = (note_data,path_to_image,callback) => {
    async.waterfall([
        (cb) => {
            try{
                db_helper.verify(note_data,["type","content","image","dateTime"]);
            }
            catch(e){
                cb(e);
            }
            cb(null,note_data);
        },
        (notes,cb) => {
            notes._id = notes.id + "_" + notes.image;
            db.notes.insertOne(notes,{w:1,safe:true},cb);
        },
        function (new_note, cb) {
            final_photo = new_note[0];

            var save_path = local.config.static_content + "albums/"
                + new_note.id + "/" + base_fn;

            backhelp.file_copy(path_to_image, save_path, true, cb);
        }
    ],callback);
}

exports.updateNote = (note_data,path_to_image,callback) => {
    async.waterfall([
        (cb) => {
            exports.getNoteById(note_data.id,cb);
        },
        (data,cb) => {
           
            db.notes.updateOne({id:data.id},{$set: {
                type : note_data.type,
                content: note_data.content,
                image : note_data.image,
                dateTime : note_data.dateTime
            }},cb);
        },
        (result,cb) => {
            cb(null,result);
        }

    ],callback);
}

exports.deleteNote = (note_data,callback) => {
    db.notes.deleteOne({id:note_data.id},callback);
}
