var mongodb = require('mongodb'),
    async = require('async'),
    local = require('../local.config')

var Db = mongodb.Db, Connection = mongodb.Connection, Server = mongodb.Server

var host = local.config.db_config.host ? local.config.db_config.host : 'localhost';
var port = local.config.db_config.port ? local.config.db_config.port : Connection.DEFAULT_PORT;
var poolsize = local.config.db_config.poolsize ? local.config.db_config.poolsize : 5;
var databaseName = local.config.db_config.database;

var db = new Db(databaseName,new Server(host,port,{auto_reconnect : true,poolSize:poolsize},{w:1}));

exports.notes = null;
exports.users = null;

exports.init = (callback) => {
    async.waterfall([
        (cb) => {
            db.open(cb);
        },
        (db_conn, cb) => {
            db.collection("notes",cb);
        },
        (notes_col,cb) => {
            exports.notes = notes_col;
            db.collection("users",cb);
        },
        (users_col,cb) => {
            exports.users = users_col;
            cb(null);
        }
    ],callback);

};