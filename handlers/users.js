var helper = require('./helper'),
    user_data = require('../data/users'),
    async = require('async'),
    bcrypt = require('bcrypt')

exports.version = "0.1.0";

function User(user_data) {
    this.uuid = user_data["user_uuid"];
    this.email_address = user_data["email_address"];
    this.name = user_data["name"];
    this.password = user_data["password"];
    this.created_dt = user_data["created_dt"];
    this.last_modified_dt = user_data["last_modified_dt"];
    this.deleted = user_data["deleted"];
}
User.prototype.response_obj = function () {
    return {
        uuid: this.uuid,
        email_address: this.email_address,
        display_name: this.display_name,
        first_seen_date: this.first_seen_date,
        last_modified_date: this.last_modified_date
    };
};
User.prototype.check_password = function (pw, callback) {
    bcrypt.compare(pw, this.password, callback);
}
exports.user_by_name = (req, res) => {
    async.waterfall([
        function (cb) {
            user_data.getUserByName(req.body.name, cb);
        }

    ], function (err, u) {
        if (!err) {
            helper.send_success(res, u.response_obj());
        }
        else {
            helper.send_failure(res, "500", err);
        }
    });
}
exports.login = (req, res) => {
    async.waterfall([
        function (cb) {
            if (!req.body.email) {
                cb(helper.missing_data("email_address"));
            } else if (req.session && req.session.logged_in_email == req.body.email) {
                cb(helper.error("logged_in_user", "already logged in"));
            }
            else {
                cb(null);
            }
        },
        function (cb) {
            user_data.getUserByEmail(req.body.email_address, cb);
        },
        function (user, cb) {
            let u = User(user);
            user_data.check_password(req.body.password, cb);
        },
        function (auth_ok, cb) {
            if (!auth_ok) {
                cb(helpers.auth_failed());
                return;
            }

            req.session.logged_in = true;
            req.session.logged_in_email_address = req.body.email_address;
            req.session.logged_in_date = new Date();
            cb(null);
        }
    ], function (err, results) {
        if (!err || err.message == "already_logged_in") {
            helpers.send_success(res, { logged_in: true });
        } else {
            helpers.send_failure(res, helpers.http_code_for_error(err), err);
        }
    });
    exports.register = (req, res) => {
        async.waterfall([
            function (cb) {
                if (!req.body.email) {
                    cb(helper.missing_data("email_address"));
                } else if (!req.body.password) {
                    cb(helper.missing_data("password"));
                }
                else if (!req.body.name) {
                    cb(helper.missing_data("name"));
                }
                else {
                    cb(null);
                }
            },
            function (cb) {
                user_data.register(req.body, cb);
            },
            function (user, cb) {
                let u = User(user);
                req.session.logged_in = true;
                req.session.logged_in_email_address = req.body.email_address;
                req.session.logged_in_date = new Date();
                cb(null);
            }
        ], function (err, results) {
            if (!err) {
                let u = User(user);
                helpers.send_success(res, { user:u.response_obj()});
            } else {
                helpers.send_failure(res, helpers.http_code_for_error(err), err);
            }
        });
    }
}