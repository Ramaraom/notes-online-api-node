var async = require('async'),
    notes = require('../data/notes'),
    helper = require('./helper'),



function Note(note_data){
    this.id = note_data.id;
    this.type = note_data.type;
    this.image = note_data.image;
    this.content = note_data.content;
    this.dateTime = note_data.dateTime;
    this.created_dt = note_data.created_dt;
    this.last_modified_dt = note_data.last_modified_dt;
}
Note.prototype.response_obj = function() {
    return {
        id : this.id,
        type : this.type,
        content:  this.content,
        image  : this.image,
        dateTime : this.dateTime
    }
}

exports.get_all_notes = (req,res) => {
    notes.getAllNotes(0,100,(err,results) =>  {
        if(err)
            helper.send_failure(res,500,err);
        else
        {
            var out = [];
            for(let i=0;i<results.length;i++){
                out.push(new Note(results[i]).response_obj());
            }
            helper.send_success(res,{notes:out});
        }
    });
}

exports.create_note = (req,res) => {
    notes.createNote(req.body.note_data,"",(err,result) => {
        if(err)
            helper.send_failure(res,500,err);
        else{
            helper.send_success(res,{});
        }
    });
}

exports.update_note = (req,res) => {
    notes.updateNote(req.body.note_data,"",(err,result) => {
        if(err)
            helper.send_failure(res,500,err);
        else{
            helper.send_success(res,{});
        }
    });
}

exports.delete_note = (req,res) => {
    notes.deleteNote(req.body.note_data,(err,result) => {
        if(err)
            helper.send_failure(res,500,err);
        else{
            helper.send_success(res,{});
        }

    });
}
