var express = require('express');
var router = express.Router();
var noteshdlr = require('../handlers/notes');

router.get('/all',noteshdlr.get_all_notes);

router.get('/note/:id',function(req,res){
    res.send({'id':req.params.id});
});
router.post('/addnote',noteshdlr.create_note);
router.delete('/deletenote',noteshdlr.delete_note);
router.update('/updatenote',noteshdlr.update_note);

module.exports = router;