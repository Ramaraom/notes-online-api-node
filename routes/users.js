var express = require('express');
var router = express.Router();
var usershdlr = require('../handlers/users');


/* GET users listing. */
router.post('/login', usershdlr.login);

router.post('/register',usershdlr.register);

router.get('/userprofile',usershdlr.user_by_name);

module.exports = router;
